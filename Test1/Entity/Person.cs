﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

namespace Test1.Entity
{
    class Person
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string SecondName { get; private set; }
        public ushort Age { get; private set; }
        public string Gender { get; private set; }
       
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="lastName">фамилия</param>
        /// <param name="firstName">имя</param>
        /// <param name="secondName">отчество</param>
        /// <param name="gender">пол</param>
        /// <param name="age">возраст</param>
        public  Person(string lastName, string firstName, string secondName, string gender, ushort age)
        {
            FirstName = firstName;
            LastName = lastName;
            SecondName = secondName;
            Gender = gender;
            
            if(age > DateTime.Now.Year) throw  new Exception("Указана некорректная дата рождения!");

            Age = age;
        }
    }

    class PersonCollection
    {
        /// <summary>
        /// загрузка из файла в коллекцию Person
        /// </summary>
        /// <returns>коллекция Person</returns>
        public static List<Person> LoadFromFile()
        {
            var data = File.ReadAllLines(Environment.CurrentDirectory + "\\PersonData.txt");

            return data.Select(line => line.Split(default(string[]),
                                        StringSplitOptions.RemoveEmptyEntries))
                  .Select(split => new Person(
                      split[0], //Lastname
                      split[1], //FirstName
                      split[2], //SecondName
                      split[3], //Gender
                      (ushort)(split[4].Length < 4 ? ushort.Parse(split[4]) : DateTime.Now.Year - ushort.Parse(split[4])))).ToList();
        }

        /// <summary>
        /// Фильтрует коллекцию Person по фильтрам из файла
        /// </summary>
        /// <param name="persons">коллекция Person</param>
        public static void Filter(List<Person> persons)
        {
            #region Загрузка фильтров из файла
            var data = File.ReadAllLines(Environment.CurrentDirectory + "\\Config.txt");

            var filterDictionary = new List<Filter>();

            foreach (var line in data)
            {
                var values = line.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries);

                if (values.Length == 1 && filterDictionary.Count > 0)
                {
                    filterDictionary.Add(new Filter
                    {
                        Condition = values[0]
                    });

                    continue;
                }

                filterDictionary.Add(new Filter
                {
                    Field = values[0],
                    Operation = values[1],
                    Operand = values[2]
                });
            }
            #endregion

            var paramName = Expression.Parameter(typeof (Person), "persons");
            Expression operation = null,
                        condition = null;

            var conditionValue = string.Empty;

            foreach (var filter in filterDictionary)
            {
                Expression curentOperation ;

                //учитывается наличие услови "И" "ИЛИ"
                if (!string.IsNullOrEmpty(filter.Condition))
                {
                    conditionValue = filter.Condition;
                    continue;
                }

                //доступ к свойствам Person, в зависимости от указанного параметра в фильтре
                Expression right = Expression.Constant(filter.Operand);

                Expression left;
                switch (filter.Field)
                {
                    case "И":
                        left = Expression.Property(paramName, typeof(Person).GetProperty("FirstName"));
                        break;
                    case "Ф":
                        left = Expression.Property(paramName, typeof(Person).GetProperty("LastName"));
                        break;
                    case "О":
                        left = Expression.Property(paramName, typeof(Person).GetProperty("SecondName"));
                        break;
                    case "П":
                        left = Expression.Property(paramName, typeof(Person).GetProperty("Gender"));
                        break;
                    case "В":
                        left = Expression.Property(paramName, typeof(Person).GetProperty("Age"));
                        right = Expression.Constant(ushort.Parse(filter.Operand));
                        break;
                    default:
                        left  = null;
                        break;
                }
                
                if(left == null ) throw new Exception("Некорректно описаны поля фильтра");

                switch (filter.Operation)
                {
                    case "=":
                        curentOperation = Expression.Equal(left, right);
                        break;
                    case "<":
                        curentOperation = Expression.LessThan(left, right);
                        break;
                    case ">":
                        curentOperation = Expression.GreaterThan(left, right);
                        break;
                    case "~":
                        var value = Expression.Constant(filter.Operand);
                        curentOperation = Expression.Call(left, typeof(string).GetMethod("Contains"),value );
                        break;
                    default:
                        curentOperation = Expression.Equal(left, right);
                        break;
                }
                
                //если было указано условие И ИЛИ в фильтре, то формируется соответсвующее выражение
                switch (conditionValue)
                {
                    case "И":
                        condition = Expression.And(operation, curentOperation);
                        operation = condition;
                        break;
                    case "ИЛИ":
                        condition = Expression.Or(operation, curentOperation);
                        operation = condition;
                        break;
                }
                if (operation != null && string.IsNullOrEmpty(conditionValue))
                {
                    condition = Expression.And(operation, curentOperation);
                    operation = condition;
                }
                if(operation == null)
                {
                    operation = curentOperation;
                }

                conditionValue = string.Empty;
            }
            
            var queryableData = persons.AsQueryable();

            var execute = Expression.Call(
                typeof(Queryable),
                "where",
                new[] { queryableData.ElementType },
                queryableData.Expression,
                Expression.Lambda<Func<Person, bool>>(condition, new[] { paramName }));

            var res = queryableData.Provider.CreateQuery<Person>(execute);

            foreach (var r in res)
            {
                Console.WriteLine("{0} {1} {2} {3} {4}", r.FirstName, r.LastName, r.SecondName, r.Age, r.Gender);
            }
            Console.WriteLine("\n\n");

            Print(res.ToList());
        }

        /// <summary>
        /// вывод на экран результатов фильтр
        /// </summary>
        /// <param name="personCollection">коллекция Person</param>
        private static void Print(List<Person> personCollection)
        {
            var gCollection = personCollection
                .GroupBy(g => g.Gender).
                Select(g => new {Gender = g.Key, Count = g.Count()});
            foreach (var i in gCollection)
            {
                var i1 = i;
                Console.WriteLine("{0} - {1} чел.", i.Gender, i.Count);

                var ageCollection = personCollection.Where(p => p.Gender == i1.Gender)
                    .GroupBy(g => g.Age)
                    .Select(g => new { Age = g.Key, Count = g.Count() });

                foreach (var item in ageCollection.OrderBy(o => o.Age))
                {
                    var item1 = item;
                    Console.WriteLine("\t{0} лет - {1} чел.", item.Age, item.Count);
                    
                    foreach (var person in personCollection.Where(p => p.Age == item1.Age))
                    {
                        Console.WriteLine("\t\t {0} {1} {2}", person.LastName, person.FirstName, person.SecondName);
                    }
                }

                Console.WriteLine();
            }
        }
    }

    struct Filter
    {
        public string Field { get; set; }
        public string Operation { get; set; }
        public string Operand { get; set; }
        public string Condition { get; set; }
    }
}
