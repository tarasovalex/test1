﻿using System;
using Test1.Entity;


namespace Test1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var pc = PersonCollection.LoadFromFile();
                PersonCollection.Filter(pc);

                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
